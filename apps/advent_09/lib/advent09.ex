defmodule Advent09 do
  @moduledoc """
  Main module of `Advent09`.
  """

  require Logger

  @doc """
  Finding exploits!

  ## Examples

      iex> Advent09.find_exploit_value([35, 20, 15, 25, 47, 40, 62, 55, 65,
      ...>                              95, 102, 117, 150, 182, 127, 219,
      ...>                              299, 277, 309, 576], 5)
      127

  """
  @spec find_exploit_value(list(integer()), integer()) :: integer()
  def find_exploit_value(sequence, preamble_length) do
    Logger.debug("Processing a sequence of length #{length(sequence)}")

    find_exploit_in_sequence(
      sequence,
      Enum.slice(sequence, 0..(preamble_length - 1)),
      1,
      preamble_length,
      true
    )
  end

  defp find_exploit_in_sequence(sequence, _subsequence, start_index, preamble_length, false) do
    Logger.debug("Found an exploit at #{start_index + preamble_length - 2}")
    Enum.at(sequence, start_index + preamble_length - 2)
  end

  defp find_exploit_in_sequence(sequence, subsequence, start_index, preamble_length, true) do
    Logger.debug(
      "Processing subsequence #{start_index - 1}..#{start_index + preamble_length - 2}"
    )

    find_exploit_in_sequence(
      sequence,
      Enum.slice(sequence, start_index..(start_index + preamble_length - 1)),
      start_index + 1,
      preamble_length,
      valid_next(subsequence, Enum.at(sequence, start_index + preamble_length - 1))
    )
  end

  @doc """
  Checking valid values.

  ## Examples

      iex> Advent09.valid_next([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
      ...>                      15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
      ...>                      26)
      true

      iex> Advent09.valid_next([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
      ...>                      15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
      ...>                      49)
      true

      iex> Advent09.valid_next([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
      ...>                      15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
      ...>                      100)
      false

      iex> Advent09.valid_next([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
      ...>                      15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
      ...>                      50)
      false

      iex> Advent09.valid_next([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
      ...>                      15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 45],
      ...>                      26)
      true

      iex> Advent09.valid_next([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
      ...>                      15, 16, 17, 18, 19, 1, 21, 22, 23, 24, 25, 45],
      ...>                      65)
      false

      iex> Advent09.valid_next([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
      ...>                      15, 16, 17, 18, 19, 1, 21, 22, 23, 24, 25, 45],
      ...>                      64)
      true

      iex> Advent09.valid_next([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
      ...>                      15, 16, 17, 18, 19, 1, 21, 22, 23, 24, 25, 45],
      ...>                      66)
      true

      iex> Advent09.valid_next([87, 109, 132, 126, 96, 108, 101, 129, 233, 103,
      ...>                      106, 172, 153, 110, 128, 175, 149, 150, 155,
      ...>                      165, 161, 181, 222, 180, 242], 422)
      true

  """
  @spec valid_next(list(integer()), integer()) :: boolean()
  def valid_next(subsequence, next) do
    Logger.debug("Checking subsequence for value #{next}")

    Enum.any?(
      subsequence,
      fn s ->
        next - s != s and Enum.member?(subsequence, next - s)
      end
    )
  end

  @doc """
  Finding exploits again!

  ## Examples

      iex> Advent09.find_exploit_sequence([35, 20, 15, 25, 47, 40, 62, 55, 65,
      ...>                                 95, 102, 117, 150, 182, 127, 219,
      ...>                                 299, 277, 309, 576], 127)
      62

  """
  @spec find_exploit_sequence(list(integer()), integer()) :: integer()
  def find_exploit_sequence(sequence, goal) do
    Logger.debug("Sequence of #{length(sequence)} elements, #{length(sequence) - 1} tasks needed")
    tasks = Enum.to_list(0..(length(sequence) - 2))

    [answer] =
      Task.async_stream(tasks, fn i ->
        first = Enum.at(sequence, i)
        second = Enum.at(sequence, i + 1)
        Logger.debug("Trying sequences from position #{i}")

        sum_range(
          i,
          sequence,
          i + 2,
          second + first,
          [second, first],
          goal
        )
      end)
      |> Enum.reduce([], fn
        {:ok, :notfound}, acc -> acc
        {:ok, range}, _acc -> [Enum.min(range) + Enum.max(range)]
      end)

    answer
  end

  @spec sum_range(integer(), list(integer()), integer(), integer(), list(integer()), integer()) ::
          list(integer())
  defp sum_range(i, _sequence, _next, goal, range, goal) do
    Logger.debug("Task #{i} reached the goal!")
    range
  end

  defp sum_range(i, sequence, next, _partial_sum, _range, _goal) when next == length(sequence) do
    Logger.debug("Task #{i} reached end and did not find suitable range")
    :notfound
  end

  defp sum_range(i, sequence, next, partial_sum, range, goal) when partial_sum < goal do
    value = Enum.at(sequence, next)
    Logger.debug("Task #{i} sums one more (#{next}=>#{value}) for #{goal} from #{partial_sum}")
    sum_range(i, sequence, next + 1, partial_sum + value, [value | range], goal)
  end

  defp sum_range(i, _sequence, _next, _partial_sum, _range, _goal) do
    Logger.debug("Task #{i} exceeded goal without finding suitable range")
    :notfound
  end
end
