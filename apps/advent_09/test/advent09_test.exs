defmodule Advent09Test do
  use ExUnit.Case
  doctest Advent09

  test "finds_answer_to_first_challenge" do
    sequence =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.to_integer(&1))
      |> Enum.to_list()

    assert Advent09.find_exploit_value(sequence, 25) == 248_131_121
  end

  test "finds_answer_to_second_challenge" do
    sequence =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.to_integer(&1))
      |> Enum.to_list()

    assert Advent09.find_exploit_sequence(sequence, 248_131_121) == 31_580_383
  end
end
