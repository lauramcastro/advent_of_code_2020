defmodule Advent06Test do
  use ExUnit.Case
  doctest Advent06

  test "finds_answer_to_first_challenge" do
    {[], answers} =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Enum.reduce({[], []}, fn
        "\n", {a, as} -> {[], [a | as]}
        line, {a, as} -> {[String.trim(line) | a], as}
      end)

    assert Advent06.count_present(answers) == 6443
  end

  test "finds_answer_to_second_challenge" do
    {[], answers} =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Enum.reduce({[], []}, fn
        "\n", {a, as} -> {[], [a | as]}
        line, {a, as} -> {[String.trim(line) | a], as}
      end)

    assert Advent06.count_present_all(answers) == 3232
  end
end
