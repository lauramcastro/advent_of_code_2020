defmodule Advent06 do
  @moduledoc """
  Main module of `Advent06`.
  """

  @doc """
  Counting positive answers to customs declarations!

  ## Examples

      iex> Advent06.count_present([["abcx", "abcy", "abcz"]])
      6

      iex> Advent06.count_present([["abc"]])
      3

      iex> Advent06.count_present([["a", "b", "c"]])
      3

      iex> Advent06.count_present([["ab", "ac"]])
      3

      iex> Advent06.count_present([["a", "a", "a", "a"]])
      1

      iex> Advent06.count_present([["b"]])
      1

      iex> Advent06.count_present([["abc"], ["a", "b", "c"], ["ab", "ac"], ["a", "a", "a", "a"], ["b"]])
      11
  """
  @spec count_present(list(list(String.t()))) :: integer()
  def count_present(answers) do
    Task.async_stream(answers, fn s -> count_letters(s) end)
    |> Enum.reduce(0, fn {:ok, sum}, acc -> sum + acc end)
  end

  @spec count_letters(list(String.t())) :: integer()
  defp count_letters(list_of_strings) do
    Enum.join(list_of_strings) |> String.graphemes() |> MapSet.new() |> MapSet.size()
  end

  @doc """
  Counting positive unanimous answers to customs declarations!

  ## Examples

      iex> Advent06.count_present_all([["abc"]])
      3

      iex> Advent06.count_present_all([["a", "b", "c"]])
      0

      iex> Advent06.count_present_all([["ab", "ac"]])
      1

      iex> Advent06.count_present_all([["a", "a", "a", "a"]])
      1

      iex> Advent06.count_present_all([["b"]])
      1

      iex> Advent06.count_present_all([["abc"], ["a", "b", "c"], ["ab", "ac"], ["a", "a", "a", "a"], ["b"]])
      6
  """
  @spec count_present_all(list(list(String.t()))) :: integer()
  def count_present_all(answers) do
    Task.async_stream(answers, fn s -> count_common_letters(s) end)
    |> Enum.reduce(0, fn {:ok, sum}, acc -> sum + acc end)
  end

  @spec count_common_letters(list(String.t())) :: integer()
  defp count_common_letters(list_of_strings) do
    [first | rest] = for s <- list_of_strings, do: String.graphemes(s) |> MapSet.new()
    Enum.reduce(rest, first, fn m, acc -> MapSet.intersection(m, acc) end) |> MapSet.size()
  end
end
