defmodule Advent01 do
  @moduledoc """
  Main module of `Advent01`.
  """
  require Logger

  @doc """
  Fixing the expenses report!

  ## Examples

      iex> Advent01.fix_report(2020, [1721, 979, 366, 299, 675, 1456])
      514579

  """
  @spec fix_report(integer(), list(integer())) :: integer() | :noanswer
  def fix_report(year, expenses) do
    Logger.debug("Processing list of expenses")
    true = Process.register(self(), :reportee)
    pids = Enum.map(expenses, fn e -> {:ok, _pid} = Expense.start_link(year, e) end)

    result =
      receive do
        answer ->
          Logger.debug("Reported matching values! #{answer}")
          answer
      after
        1000 -> :noanswer
      end

    :ok = Enum.each(pids, fn {:ok, p} -> :ok = Expense.stop(p) end)
    true = Process.unregister(:reportee)

    result
  end

  @doc """
  Fixing the expenses report (now with triplets)!

  ## Examples

      iex> Advent01.fix_report_with_triplets(2020, [1721, 979, 366, 299, 675, 1456])
      241861950

  """
  @spec fix_report_with_triplets(integer(), list(integer())) :: integer()
  def fix_report_with_triplets(year, [e | expenses]) do
    find_triplet(year, expenses, [e], fix_report(year - e, expenses))
  end

  @spec find_triplet(integer(), list(integer()), list(integer()), integer() | :noanswer) ::
          integer() | :noanswer
  defp find_triplet(_, [], _, :noanswer), do: :noanswer
  defp find_triplet(_, _, [e | _], value) when is_integer(value), do: e * value

  defp find_triplet(year, [e | rem_expenses], prev_expenses, :noanswer) do
    find_triplet(
      year,
      rem_expenses,
      [e | prev_expenses],
      fix_report(year - e, rem_expenses ++ prev_expenses)
    )
  end
end
