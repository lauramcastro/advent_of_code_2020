defmodule Expense do
  @moduledoc """
  A GenServer process that represents an expense.
  """
  use GenServer
  require Logger

  # API
  @spec start_link(integer(), integer()) :: {:ok, pid()}
  def start_link(year, value) when is_integer(value) and value > 0 do
    GenServer.start_link(__MODULE__, [year, value], name: String.to_atom("#{value}"))
  end

  @spec stop(pid()) :: :ok
  def stop(expense) do
    stop_if_alive(expense, Process.alive?(expense))
  end

  # Callbacks

  @impl true
  def init([year, value]) do
    Logger.debug("Creating process #{value}")
    schedule_search()
    {:ok, {year, value}}
  end

  @impl true
  def handle_info({:match, match_value}, {year, value}) do
    Logger.debug("Received message from match (#{match_value}, #{value})! Reporting...")
    Process.send(:reportee, match_value * value, [:nosuspend])
    {:stop, :normal, {year, value}}
  end

  def handle_info(:search_match, {year, value}) do
    potential_match = year - value
    Logger.debug("Searching for match of process #{value}, who would be #{potential_match}")
    find_match(potential_match, value, Process.whereis(String.to_atom("#{potential_match}")))
    {:noreply, {year, value}}
  end

  @impl true
  def terminate(:normal, {_year, value}) do
    Logger.debug("Shutting down process #{value}")
    :ok
  end

  # Internal functions

  @spec schedule_search() :: reference()
  defp schedule_search() do
    Process.send_after(self(), :search_match, trunc(:rand.uniform() * 1_000))
  end

  @spec find_match(integer(), integer(), nil | pid()) ::
          :no_match_alive | :ok | :noconnect | :nosuspend
  defp find_match(potential_match, _value, nil) do
    Logger.debug("No #{potential_match} alive")
    :no_match_alive
  end

  defp find_match(potential_match, value, pid) when is_pid(pid) do
    Logger.debug("Found #{potential_match} alive!")
    Process.send(String.to_atom("#{potential_match}"), {:match, value}, [:nosuspend])
  end

  @spec stop_if_alive(pid(), boolean()) :: :ok
  defp stop_if_alive(_pid, false), do: :ok
  defp stop_if_alive(pid, true), do: GenServer.stop(pid)
end
