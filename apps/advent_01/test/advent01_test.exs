defmodule Advent01Test do
  use ExUnit.Case
  doctest Advent01

  test "finds_answer_to_first_challenge" do
    expenses =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.to_integer(&1))

    assert Advent01.fix_report(2020, expenses) == 41_979
  end

  test "finds_answer_to_second_challenge" do
    expenses =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.to_integer(&1))
      |> Enum.to_list()

    assert Advent01.fix_report_with_triplets(2020, expenses) == 193_416_912
  end
end
