defmodule Advent08 do
  @moduledoc """
  Main module of `Advent08`.
  """

  @doc """
  Running code!

  ## Examples

      iex> Advent08.get_accumulator_before_loop(["nop +0",
      ...>                                       "acc +1",
      ...>                                       "jmp +4",
      ...>                                       "acc +3",
      ...>                                       "jmp -3",
      ...>                                       "acc -99",
      ...>                                       "acc +1",
      ...>                                       "jmp -4",
      ...>                                       "acc +6"])
      5

  """
  @spec get_accumulator_before_loop(list(String.t())) :: integer()
  def get_accumulator_before_loop(program) do
    compute_until(program, length(program), 0, {0, []}, &has_duplicates/2, false)
  end

  @doc """
  Running code!

  ## Examples

      iex> Advent08.get_accumulator_at_eop(["nop +0",
      ...>                                  "acc +1",
      ...>                                  "jmp +4",
      ...>                                  "acc +3",
      ...>                                  "jmp -3",
      ...>                                  "acc -99",
      ...>                                  "acc +1",
      ...>                                  "jmp -4",
      ...>                                  "acc +6"])
      8

  """
  @spec get_accumulator_at_eop(list(String.t())) :: integer()
  def get_accumulator_at_eop(program) do
    {indexed_program, program_length} =
      Enum.map_reduce(program, 0, fn instruction, acc ->
        {{acc, String.split(instruction)}, acc + 1}
      end)

    patches =
      Enum.filter(indexed_program, fn
        {_i, ["jmp", _val]} -> true
        {_i, ["nop", _val]} -> true
        _ -> false
      end)

    # spawn one task per patched program, only one will return, and that is the answer
    [{_task, {:ok, answer}}] =
      patches
      |> Enum.map(fn {i, inst} ->
        Task.async(fn ->
          patch(program, i, inst)
          |> compute_until(program_length, 0, {0, []}, &eop/2, false)
        end)
      end)
      |> Task.yield_many()
      |> Enum.filter(fn
        {_task, {:ok, _reply}} -> true
        {_task, nil} -> false
      end)

    answer
  end

  @spec patch(list(String.t()), integer(), list()) :: list(String.t())
  defp patch(program, i, ["jmp", value]), do: List.replace_at(program, i, ["nop", value])
  defp patch(program, i, ["nop", value]), do: List.replace_at(program, i, ["jmp", value])

  @spec compute_until(
          list(String.t()),
          integer(),
          integer(),
          {integer(), list(integer())},
          term(),
          boolean()
        ) :: integer()
  defp compute_until(_program, _program_length, _pointer, {acc, _history}, _halt_criteria, true),
    do: acc

  defp compute_until(program, program_length, pointer, {acc, history}, halt_criteria, false) do
    instruction = Enum.at(program, pointer)
    {new_pointer, new_acc} = execute(instruction, pointer, acc)
    new_history = [pointer | history]

    compute_until(
      program,
      program_length,
      new_pointer,
      {new_acc, new_history},
      halt_criteria,
      halt_criteria.(program_length, [new_pointer | history])
    )
  end

  @spec execute(String.t() | list(String.t()), integer(), integer()) :: {integer(), integer()}
  defp execute(instruction, pointer, acc) when is_list(instruction),
    do: do_execute(instruction, pointer, acc)

  defp execute(instruction, pointer, acc), do: do_execute(String.split(instruction), pointer, acc)

  @spec do_execute(list(String.t()), integer(), integer()) :: {integer(), integer()}
  defp do_execute(["nop", _value], pointer, acc), do: {pointer + 1, acc}
  defp do_execute(["acc", value], pointer, acc), do: {pointer + 1, acc + String.to_integer(value)}
  defp do_execute(["jmp", value], pointer, acc), do: {pointer + String.to_integer(value), acc}

  @spec has_duplicates(integer(), list()) :: boolean()
  defp has_duplicates(_, []), do: false
  defp has_duplicates(_, list), do: MapSet.new(list) |> MapSet.size() != length(list)

  @spec eop(integer(), list()) :: boolean()
  defp eop(program_length, [program_length | _]), do: true
  defp eop(_program_length, [_pointer | _history]), do: false
end
