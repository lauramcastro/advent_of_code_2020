defmodule Advent08Test do
  use ExUnit.Case
  doctest Advent08

  test "finds_answer_to_first_challenge" do
    program =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Enum.to_list()

    assert Advent08.get_accumulator_before_loop(program) == 1753
  end

  test "finds_answer_to_second_challenge" do
    program =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Enum.to_list()

    assert Advent08.get_accumulator_at_eop(program) == 733
  end
end
