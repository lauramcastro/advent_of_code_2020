defmodule Advent13 do
  @moduledoc """
  Main module of `Advent13`.
  """

  require Logger

  @doc """
  Finding buses to ride!

  ## Examples

      iex> Advent13.next_bus(939, ["7","13","x","x","59","x","31","19"])
      295

  """
  @spec next_bus(pos_integer(), list(String.t())) :: pos_integer()
  def next_bus(timestamp, buses) do
    {id, time} =
      Task.async_stream(buses, fn
        "x" ->
          {"x", :infinity}

        bus_id ->
          value = String.to_integer(bus_id)
          wait = value - rem(timestamp, value)
          Logger.debug("Bus " <> bus_id <> " will still take #{wait} minutes")
          {bus_id, wait}
      end)
      |> Enum.reduce({"x", :infinity}, fn
        {:ok, {"x", :infinity}}, acc ->
          acc

        {:ok, {bus, time_to_bus}}, {_prior_bus, time_to_prior} when time_to_bus < time_to_prior ->
          {String.to_integer(bus), time_to_bus}

        {:ok, {_bus, _time_to_bus}}, acc ->
          acc
      end)

    id * time
  end
end
