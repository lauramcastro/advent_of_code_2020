defmodule Advent13Test do
  use ExUnit.Case
  doctest Advent13

  test "finds_answer_to_first_challenge" do
    [timestamp, buses] =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()

    assert Advent13.next_bus(String.to_integer(timestamp), String.split(buses, ",")) == 2995
  end
end
