defmodule Advent03 do
  @moduledoc """
  Main module of `Advent03`.
  """

  @doc """
  Counting trees!

  ## Examples

      iex> Advent03.tree_count(
      ...> ["..##.......",
      ...>  "#...#...#..",
      ...>  ".#....#..#.",
      ...>  "..#.#...#.#",
      ...>  ".#...##..#.",
      ...>  "..#.##.....",
      ...>  ".#.#.#....#",
      ...>  ".#........#",
      ...>  "#.##...#...",
      ...>  "#...##....#",
      ...>  ".#..#...#.#"])
      7
  """
  @spec tree_count(list(String.t())) :: integer()
  def tree_count(forest) do
    parametric_tree_count({:right, 3}, {:down, 1}, forest)
  end

  @doc """
  Parametrized tree count!

  ## Examples

      iex> Advent03.parametric_tree_count({:right, 1}, {:down, 1},
      ...> ["..##.......",
      ...>  "#...#...#..",
      ...>  ".#....#..#.",
      ...>  "..#.#...#.#",
      ...>  ".#...##..#.",
      ...>  "..#.##.....",
      ...>  ".#.#.#....#",
      ...>  ".#........#",
      ...>  "#.##...#...",
      ...>  "#...##....#",
      ...>  ".#..#...#.#"])
      2

      iex> Advent03.parametric_tree_count({:right, 5}, {:down, 1},
      ...> ["..##.......",
      ...>  "#...#...#..",
      ...>  ".#....#..#.",
      ...>  "..#.#...#.#",
      ...>  ".#...##..#.",
      ...>  "..#.##.....",
      ...>  ".#.#.#....#",
      ...>  ".#........#",
      ...>  "#.##...#...",
      ...>  "#...##....#",
      ...>  ".#..#...#.#"])
      3

      iex> Advent03.parametric_tree_count({:right, 7}, {:down, 1},
      ...> ["..##.......",
      ...>  "#...#...#..",
      ...>  ".#....#..#.",
      ...>  "..#.#...#.#",
      ...>  ".#...##..#.",
      ...>  "..#.##.....",
      ...>  ".#.#.#....#",
      ...>  ".#........#",
      ...>  "#.##...#...",
      ...>  "#...##....#",
      ...>  ".#..#...#.#"])
      4

      iex> Advent03.parametric_tree_count({:right, 1}, {:down, 2},
      ...> ["..##.......",
      ...>  "#...#...#..",
      ...>  ".#....#..#.",
      ...>  "..#.#...#.#",
      ...>  ".#...##..#.",
      ...>  "..#.##.....",
      ...>  ".#.#.#....#",
      ...>  ".#........#",
      ...>  "#.##...#...",
      ...>  "#...##....#",
      ...>  ".#..#...#.#"])
      2
  """
  @spec parametric_tree_count({atom(), integer()}, {atom(), integer()}, list(String.t())) ::
          integer()
  def parametric_tree_count({:right, r}, {:down, d}, forest) do
    binary_forest = for row <- forest, do: String.graphemes(row) |> Enum.map(&turn_binary(&1))

    forest_matrix = Matrex.new(binary_forest)
    # Matrex.print(forest_matrix)

    count_trees(
      {:down, d},
      {:right, r},
      forest_matrix,
      Matrex.size(forest_matrix),
      1 + d,
      1 + r,
      0.0
    )
    |> round
  end

  @doc """
  Multiple parametrized tree count!

  ## Examples

      iex> Advent03.multiple_tree_count(
      ...> [[{:right, 1}, {:down, 1}],
      ...>  [{:right, 3}, {:down, 1}],
      ...>  [{:right, 5}, {:down, 1}],
      ...>  [{:right, 7}, {:down, 1}],
      ...>  [{:right, 1}, {:down, 2}]],
      ...> ["..##.......",
      ...>  "#...#...#..",
      ...>  ".#....#..#.",
      ...>  "..#.#...#.#",
      ...>  ".#...##..#.",
      ...>  "..#.##.....",
      ...>  ".#.#.#....#",
      ...>  ".#........#",
      ...>  "#.##...#...",
      ...>  "#...##....#",
      ...>  ".#..#...#.#"])
      336
  """
  @spec multiple_tree_count(list(list({atom(), integer()})), list(String.t())) ::
          integer()
  def multiple_tree_count(slopes, forest) do
    slopes
    |> Enum.map(fn [right, down] -> parametric_tree_count(right, down, forest) end)
    |> Enum.reduce(fn x, acc -> x * acc end)
  end

  @spec turn_binary(String.t()) :: integer()
  defp turn_binary("."), do: 0
  defp turn_binary("#"), do: 1

  @spec count_trees(
          {atom(), integer()},
          {atom(), integer()},
          Matrex.t(),
          {integer(), integer()},
          integer(),
          integer(),
          float()
        ) :: float()
  defp count_trees(_down, _right, _m, {rows, _columns}, i, _j, acc) when i > rows do
    acc
  end

  defp count_trees({:down, d}, {:right, r}, m, {rows, columns}, i, j, acc) when j > columns,
    do: count_trees({:down, d}, {:right, r}, m, {rows, columns}, i, j - columns, acc)

  defp count_trees({:down, d}, {:right, r}, m, {rows, columns}, i, j, acc) do
    tree = Matrex.at(m, i, j)
    count_trees({:down, d}, {:right, r}, m, {rows, columns}, i + d, j + r, acc + tree)
  end
end
