defmodule Advent03Test do
  use ExUnit.Case
  doctest Advent03

  test "finds_answer_to_first_challenge" do
    forest =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))

    assert Advent03.tree_count(forest) == 262
  end

  test "finds_answer_to_second_challenge" do
    forest =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))

    slopes = [
      [{:right, 1}, {:down, 1}],
      [{:right, 3}, {:down, 1}],
      [{:right, 5}, {:down, 1}],
      [{:right, 7}, {:down, 1}],
      [{:right, 1}, {:down, 2}]
    ]

    assert Advent03.multiple_tree_count(slopes, forest) == 2_698_900_776
  end
end
