defmodule Advent10 do
  @moduledoc """
  Main module of `Advent10`.
  """

  @doc """

  Classifying joltage differences!

  ## Examples

      iex> Advent10.count_joltages([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4])
      [{:onejolt, 7}, {:twojolts, 0}, {:threejolts, 5}]

      iex> Advent10.count_joltages([28, 33, 18, 42, 31, 14, 46, 20, 48, 47,
      ...>                          24, 23, 49, 45, 19, 38, 39, 11, 1, 32,
      ...>                          25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3])
      [{:onejolt, 22}, {:twojolts, 0}, {:threejolts, 10}]
  """
  @spec count_joltages(list(integer())) :: list({:onejolt | :twojolts | :threejolts, integer()})
  def count_joltages(adapters) do
    # charging outlet of 0 at beginning
    Enum.sort([0 | adapters])
    # built-in adapter +3 at the end
    |> count_joltages([{:onejolt, 0}, {:twojolts, 0}, {:threejolts, 1}])
  end

  @spec count_joltages(list(integer()), list({:onejolt | :twojolts | :threejolts, integer()})) ::
          list({:onejolt | :twojolts | :threejolts, integer()})
  defp count_joltages([_last], counts), do: counts

  defp count_joltages([element, another_element | rest], counts),
    do:
      count_joltages(
        [another_element | rest],
        increment_joltage(another_element - element, counts)
      )

  @spec increment_joltage(integer(), list({:onejolt | :twojolts | :threejolts, integer()})) ::
          list({:onejolt | :twojolts | :threejolts, integer()})
  defp increment_joltage(1, [{:onejolt, ones}, {:twojolts, twos}, {:threejolts, threes}]),
    do: [{:onejolt, ones + 1}, {:twojolts, twos}, {:threejolts, threes}]

  defp increment_joltage(2, [{:onejolt, ones}, {:twojolts, twos}, {:threejolts, threes}]),
    do: [{:onejolt, ones}, {:twojolts, twos + 1}, {:threejolts, threes}]

  defp increment_joltage(3, [{:onejolt, ones}, {:twojolts, twos}, {:threejolts, threes}]),
    do: [{:onejolt, ones}, {:twojolts, twos}, {:threejolts, threes + 1}]

  @doc """

  Counting joltage arrangements!

  ## Examples

      iex> Advent10.count_joltage_arrangements([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4])
      8

      iex> Advent10.count_joltage_arrangements([28, 33, 18, 42, 31, 14, 46, 20, 48, 47,
      ...>                                      24, 23, 49, 45, 19, 38, 39, 11, 1, 32,
      ...>                                      25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3])
      19208

  """
  @spec count_joltage_arrangements(list(integer())) :: integer()
  def count_joltage_arrangements(adapters) do
    # charging outlet of 0 at beginning cannot be removed
    {s1, s2, s3} = Enum.sort([0 | adapters]) |> count_sequences({0, 0, 0})

    round(
      :math.pow(combinations(1) + 1, s1) * :math.pow(combinations(2) + 1, s2) *
        :math.pow(combinations(3), s3)
    )
  end

  @spec count_sequences(list(integer()), {integer(), integer(), integer()}) ::
          {integer(), integer(), integer()}
  def count_sequences([], acc), do: acc

  def count_sequences([a, b, c, d, e | rest], {acc1, acc2, acc3})
      when a + 1 == b and b + 1 == c and c + 1 == d and d + 1 == e,
      do: count_sequences(rest, {acc1, acc2, acc3 + 1})

  def count_sequences([a, b, c, d | rest], {acc1, acc2, acc3})
      when a + 1 == b and b + 1 == c and c + 1 == d,
      do: count_sequences(rest, {acc1, acc2 + 1, acc3})

  def count_sequences([a, b, c | rest], {acc1, acc2, acc3}) when a + 1 == b and b + 1 == c,
    do: count_sequences(rest, {acc1 + 1, acc2, acc3})

  def count_sequences([_ | rest], acc),
    do: count_sequences(rest, acc)

  @spec combinations(integer()) :: integer()
  def combinations(n), do: combinations(n, 1, 0)

  defp combinations(n, n, acc), do: acc + 1

  defp combinations(n, 1, acc), do: combinations(n, 2, acc + n)

  defp combinations(n, m, acc) do
    combinations(n, m + 1, acc + round(factorial(n) / (factorial(m) * factorial(n - m))))
  end

  @spec factorial(integer()) :: integer()
  def factorial(n), do: do_factorial(n, 1)

  defp do_factorial(0, acc), do: acc
  defp do_factorial(n, acc), do: do_factorial(n - 1, n * acc)
end
