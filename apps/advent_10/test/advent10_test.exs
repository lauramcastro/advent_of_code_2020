defmodule Advent10Test do
  use ExUnit.Case
  doctest Advent10

  test "finds_answer_to_first_challenge" do
    adapters =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.to_integer(&1))
      |> Enum.to_list()

    [{:onejolt, ones}, {:twojolts, _twos}, {:threejolts, threes}] =
      Advent10.count_joltages(adapters)

    assert ones * threes == 2_272
  end

  test "finds_answer_to_second_challenge" do
    adapters =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.to_integer(&1))
      |> Enum.to_list()

    assert Advent10.count_joltage_arrangements(adapters) == 84_627_647_627_264
  end
end
