defmodule Advent04 do
  @moduledoc """
  Main module of `Advent04`.
  """

  @doc """
  Counting valid passports!

  ## Examples

      iex> Advent04.valid_passports(
      ...>   [["ecl:gry pid:860033327 eyr:2020 hcl:#fffffd"],
      ...>    ["byr:1937 iyr:2017 cid:147 hgt:183cm"],
      ...>    [],
      ...>    ["iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884"],
      ...>    ["hcl:#cfa07d byr:1929"],
      ...>    [],
      ...>    ["hcl:#ae17e1 iyr:2013"],
      ...>    ["eyr:2024"],
      ...>    ["ecl:brn pid:760753108 byr:1931"],
      ...>    ["hgt:179cm"],
      ...>    [],
      ...>    ["hcl:#cfa07d eyr:2025 pid:166559648"],
      ...>    ["iyr:2011 ecl:brn hgt:59in"]])
      2

  """
  @spec valid_passports(list(String.t()), boolean()) :: integer()
  def valid_passports(passports, strict \\ false) do
    passports
    |> Enum.chunk_while(
      [],
      fn
        [], acc -> {:cont, acc, []}
        passport_line, acc -> {:cont, passport_line ++ acc}
      end,
      fn
        [] -> {:cont, []}
        passport -> {:cont, passport, []}
      end
    )
    |> Enum.map(fn p -> parse_passport(p) end)
    |> Enum.map(fn p ->
      is_valid?(p, ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], strict)
    end)
    |> Enum.reduce(0, fn
      true, acc -> acc + 1
      false, acc -> acc
    end)
  end

  @spec parse_passport(list(String.t())) :: map()
  defp parse_passport(p) do
    Enum.join(p, " ")
    |> String.split(" ")
    |> Enum.map(&String.split(&1, ":"))
    |> Enum.map(fn [a, b] -> {a, b} end)
    |> Map.new()
  end

  @doc """
  Validating passports

  ## Examples

      iex> Advent04.is_valid?(%{"eyr" => "1972", "cid" => "100", "hcl" => "#18171d", "ecl" => "amb",
      ...>                      "hgt" => "170", "pid" => "186cm", "iyr" => "2018", "byr" => "1926"},
      ...>                    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], true)
      false

      iex> Advent04.is_valid?(%{"iyr" => "2019", "hcl" => "#602927", "eyr" => "1967", "hgt" => "170cm",
      ...>                      "ecl" => "grn", "pid" => "012533040", "byr" => "1946"},
      ...>                    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], true)
      false

      iex> Advent04.is_valid?(%{"hcl" => "dab227", "iyr" => "2012", "ecl" => "brn", "hgt" => "182cm",
      ...>                      "pid" => "021572410", "eyr" => "2020", "byr" => "1992", "cid" => "277"},
      ...>                    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], true)
      false

      iex> Advent04.is_valid?(%{"hgt" => "59cm", "ecl" => "zzz", "eyr" => "2038", "hcl" => "74454a",
      ...>                      "iyr" => "2023" , "pid" => "3556412378", "byr" => "2007"},
      ...>                    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], true)
      false

      iex> Advent04.is_valid?(%{"pid" => "087499704", "hgt" => "74in", "ecl" => "grn", "iyr" => "2012",
      ...>                      "eyr" => "2030", "byr" => "1980", "hcl" => "#623a2f"},
      ...>                    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], true)
      true

      iex> Advent04.is_valid?(%{"eyr" => "2029", "ecl" => "blu", "cid" => "129", "byr" => "1989",
      ...>                      "iyr" => "2014", "pid" => "896056539", "hcl" => "#a97842", "hgt" => "165cm"},
      ...>                    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], true)
      true

      iex> Advent04.is_valid?(%{"hcl" => "#888785", "hgt" => "164cm", "byr" => "2001", "iyr" => "2015",
      ...>                      "cid" => "88", "pid" => "545766238", "ecl" => "hzl", "eyr" => "2022"},
      ...>                    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], true)
      true

      iex> Advent04.is_valid?(%{"iyr" => "2010", "hgt" => "158cm", "hcl" => "#b6652a", "ecl" => "blu",
      ...>                      "byr" => "1944", "eyr" => "2021", "pid" => "093154719"},
      ...>                    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"], true)
      true

  """
  @spec is_valid?(map(), list(String.t()), boolean()) :: boolean()
  def is_valid?(p, compulsory, false), do: Enum.all?(compulsory, fn c -> Map.has_key?(p, c) end)

  def is_valid?(p, compulsory, true),
    do: Enum.all?(compulsory, fn c -> is_field_valid?(c, Map.get(p, c)) end)

  @doc """
  Validating passport fields

  ## Examples

      iex> Advent04.is_field_valid?("byr", "2002")
      true
      iex> Advent04.is_field_valid?("byr", "2003")
      false
      iex> Advent04.is_field_valid?("hgt", "60in")
      true   
      iex> Advent04.is_field_valid?("hgt", "190cm")
      true
      iex> Advent04.is_field_valid?("hgt", "190in")
      false
      iex> Advent04.is_field_valid?("hgt", "190")
      false
      iex> Advent04.is_field_valid?("hcl", "#123abc")
      true
      iex> Advent04.is_field_valid?("hcl", "#123abz")
      false
      iex> Advent04.is_field_valid?("hcl", "123abc")
      false
      iex> Advent04.is_field_valid?("ecl", "brn")
      true
      iex> Advent04.is_field_valid?("ecl", "wat")
      false
      iex> Advent04.is_field_valid?("pid", "000000001")
      true
      iex> Advent04.is_field_valid?("pid", "0123456789")
      false

  """
  @spec is_field_valid?(String.t(), String.t() | nil) :: boolean()
  def is_field_valid?(_c, nil), do: false

  def is_field_valid?("byr", value) do
    String.length(value) == 4 and value >= "1920" and value <= "2002"
  end

  def is_field_valid?("iyr", value) do
    String.length(value) == 4 and value >= "2010" and value <= "2020"
  end

  def is_field_valid?("eyr", value) do
    String.length(value) == 4 and value >= "2020" and value <= "2030"
  end

  def is_field_valid?("hgt", value) do
    is_valid_imperial(String.ends_with?(value, "in"), value) or
      is_valid_international(String.ends_with?(value, "cm"), value)
  end

  def is_field_valid?("hcl", value) do
    String.starts_with?(value, "#") and String.length(value) == 7 and
      String.replace(value, ~r/^#[[:digit:](a-f)]+/, "") == ""
  end

  def is_field_valid?("ecl", "amb"), do: true
  def is_field_valid?("ecl", "blu"), do: true
  def is_field_valid?("ecl", "brn"), do: true
  def is_field_valid?("ecl", "gry"), do: true
  def is_field_valid?("ecl", "grn"), do: true
  def is_field_valid?("ecl", "hzl"), do: true
  def is_field_valid?("ecl", "oth"), do: true
  def is_field_valid?("ecl", _value), do: false

  def is_field_valid?("pid", value) do
    String.length(value) == 9 and String.replace(value, ~r/[[:digit:]]+/, "") == ""
  end

  @spec is_valid_imperial(boolean(), String.t()) :: boolean()
  defp is_valid_imperial(false, _value), do: false

  defp is_valid_imperial(true, value) do
    actual_value = String.replace_suffix(value, "in", "")
    actual_value >= "59" and actual_value <= "76"
  end

  @spec is_valid_international(boolean(), String.t()) :: boolean()
  defp is_valid_international(false, _value), do: false

  defp is_valid_international(true, value) do
    actual_value = String.replace_suffix(value, "cm", "")
    actual_value >= "150" and actual_value <= "193"
  end
end
