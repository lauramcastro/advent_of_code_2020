defmodule Advent04Test do
  use ExUnit.Case
  doctest Advent04

  test "finds_answer_to_first_challenge" do
    passports =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.split(&1, ~r{\n}, trim: true))
      |> Enum.to_list()

    assert Advent04.valid_passports(passports) == 170
  end

  test "finds_answer_to_second_challenge" do
    passports =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.split(&1, ~r{\n}, trim: true))
      |> Enum.to_list()

    assert Advent04.valid_passports(passports, true) == 103
  end
end
