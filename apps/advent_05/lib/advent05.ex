defmodule Advent05 do
  @moduledoc """
  Main module of `Advent05`.
  """

  @doc """
  Finding seat IDs!

  ## Examples

      iex> Advent05.find_seat("FBFBBFFRLR")
      357

      iex> Advent05.find_seat("BFFFBBFRRR")
      567

      iex> Advent05.find_seat("FFFBBBFRRR")
      119

      iex> Advent05.find_seat("BBFFBBFRLL")
      820

  """
  @spec find_seat(String.t()) :: integer()
  def find_seat(code) do
    find_seat(0, 127, 0, 7, String.graphemes(code))
  end

  @doc """
  Finding highest seat ID!
  """
  @spec find_highest_seat(list(String.t())) :: integer()
  def find_highest_seat(list_of_codes) do
    {:ok, highest} =
      Task.async_stream(list_of_codes, fn s -> find_seat(s) end)
      |> Enum.max()

    highest
  end

  @doc """
  Finding the available seat ID in the middle of the plane!
  """
  @spec find_my_seat(list(String.t())) :: integer()
  def find_my_seat(list_of_codes) do
    available = find_available_seats(list_of_codes)
    # discard front and back rows
    [myseat] =
      Enum.reduce(available, [], fn
        id, acc when id < 100 -> acc
        id, acc when id > 900 -> acc
        id, acc -> [id | acc]
      end)

    myseat
  end

  @doc """
  Finding available seat IDs!
  """
  @spec find_available_seats(list(String.t())) :: list(integer())
  def find_available_seats(list_of_codes) do
    present =
      Task.async_stream(list_of_codes, fn s -> find_seat(s) end)
      |> Enum.reduce([], fn {:ok, id}, acc -> [id | acc] end)

    available = Enum.to_list(1..(128 * 8)) -- present
    Enum.sort(available)
  end

  @spec find_seat(integer(), integer(), integer(), integer(), list(String.t())) :: integer()
  defp find_seat(row, row, column, column, []) do
    row * 8 + column
  end

  defp find_seat(lower_bound_row, upper_bound_row, lower_bound_column, upper_bound_column, [
         "F" | rest_of_code
       ]) do
    find_seat(
      lower_bound_row,
      upper_bound_row - floor((upper_bound_row - lower_bound_row + 1) / 2),
      lower_bound_column,
      upper_bound_column,
      rest_of_code
    )
  end

  defp find_seat(lower_bound_row, upper_bound_row, lower_bound_column, upper_bound_column, [
         "B" | rest_of_code
       ]) do
    find_seat(
      lower_bound_row + floor((upper_bound_row - lower_bound_row + 1) / 2),
      upper_bound_row,
      lower_bound_column,
      upper_bound_column,
      rest_of_code
    )
  end

  defp find_seat(row, row, lower_bound_column, upper_bound_column, ["L" | rest_of_code]) do
    find_seat(
      row,
      row,
      lower_bound_column,
      upper_bound_column - floor((upper_bound_column - lower_bound_column + 1) / 2),
      rest_of_code
    )
  end

  defp find_seat(row, row, lower_bound_column, upper_bound_column, ["R" | rest_of_code]) do
    find_seat(
      row,
      row,
      lower_bound_column + floor((upper_bound_column - lower_bound_column + 1) / 2),
      upper_bound_column,
      rest_of_code
    )
  end
end
