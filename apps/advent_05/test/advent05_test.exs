defmodule Advent05Test do
  use ExUnit.Case
  doctest Advent05

  test "finds_answer_to_first_challenge" do
    seats =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()

    assert Advent05.find_highest_seat(seats) == 951
  end

  test "finds_answer_to_second_challenge" do
    seats =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()

    assert Advent05.find_my_seat(seats) == 653
  end
end
