defmodule Advent12Test do
  use ExUnit.Case
  doctest Advent12

  test "finds_answer_to_first_challenge" do
    steps =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()

    assert Advent12.manhattan_distance_after(steps, :ship) == 796
  end

  test "finds_answer_to_second_challenge" do
    steps =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()

    assert Advent12.manhattan_distance_after(steps, :waypoint) == 39_446
  end
end
