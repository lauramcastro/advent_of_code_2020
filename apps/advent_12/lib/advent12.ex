defmodule Advent12 do
  @moduledoc """
  Main module of `Advent12`.
  """

  require Logger

  @doc """
  Navigating a ferry!

  ## Examples

      iex> Advent12.manhattan_distance_after(["F10", "N3", "F7", "R90", "F11"], :ship)
      25

      iex> Advent12.manhattan_distance_after(["F10", "N3", "F7", "R90", "F11"], :waypoint)
      286

  """
  @spec manhattan_distance_after(list(String.t()), atom()) :: pos_integer()
  def manhattan_distance_after(steps, :ship) do
    {x, y, _direction} = navigate_ship(0, 0, "E", steps)
    abs(x) + abs(y)
  end

  def manhattan_distance_after(steps, :waypoint) do
    {x, y} = navigate_waypoint(0, 0, 10, 1, steps)
    abs(x) + abs(y)
  end

  @spec navigate_ship(integer(), integer(), String.t(), list(String.t())) ::
          {integer(), integer(), String.t()}
  defp navigate_ship(x, y, direction, []), do: {x, y, direction}

  defp navigate_ship(x, y, direction, [move | rest_of_moves]) do
    {command, value} = String.split_at(move, 1)
    {new_x, new_y, new_direction} = step(x, y, direction, command, String.to_integer(value))

    Logger.debug(
      "After moving #{value} from #{x},#{y} facing #{direction}, we are at #{new_x},#{new_y} facing #{
        new_direction
      }"
    )

    navigate_ship(new_x, new_y, new_direction, rest_of_moves)
  end

  @spec step(integer(), integer(), String.t(), String.t(), integer()) ::
          {integer(), integer(), String.t()}
  defp step(x, y, direction, "N", amount), do: {x, y + amount, direction}
  defp step(x, y, direction, "S", amount), do: {x, y - amount, direction}
  defp step(x, y, direction, "E", amount), do: {x + amount, y, direction}
  defp step(x, y, direction, "W", amount), do: {x - amount, y, direction}
  defp step(x, y, "N", "R", 90), do: {x, y, "E"}
  defp step(x, y, "N", "R", 180), do: {x, y, "S"}
  defp step(x, y, "N", "R", 270), do: {x, y, "W"}
  defp step(x, y, "S", "R", 90), do: {x, y, "W"}
  defp step(x, y, "S", "R", 180), do: {x, y, "N"}
  defp step(x, y, "S", "R", 270), do: {x, y, "E"}
  defp step(x, y, "E", "R", 90), do: {x, y, "S"}
  defp step(x, y, "E", "R", 180), do: {x, y, "W"}
  defp step(x, y, "E", "R", 270), do: {x, y, "N"}
  defp step(x, y, "W", "R", 90), do: {x, y, "N"}
  defp step(x, y, "W", "R", 180), do: {x, y, "E"}
  defp step(x, y, "W", "R", 270), do: {x, y, "S"}
  defp step(x, y, direction, "L", 90), do: step(x, y, direction, "R", 270)
  defp step(x, y, direction, "L", 180), do: step(x, y, direction, "R", 180)
  defp step(x, y, direction, "L", 270), do: step(x, y, direction, "R", 90)
  defp step(x, y, direction, "F", amount), do: step(x, y, direction, direction, amount)

  @spec navigate_waypoint(integer(), integer(), integer(), integer(), list(String.t())) ::
          {integer(), integer()}
  defp navigate_waypoint(ship_x, ship_y, _x, _y, []), do: {ship_x, ship_y}

  defp navigate_waypoint(ship_x, ship_y, x, y, [move | rest_of_moves]) do
    {command, value} = String.split_at(move, 1)

    {new_ship_x, new_ship_y, new_x, new_y} =
      step_waypoint(ship_x, ship_y, x, y, command, String.to_integer(value))

    Logger.debug(
      "With ship at #{ship_x},#{ship_y} (waypoint at #{x},#{y}) we move #{value} times to #{
        new_ship_x
      },#{new_ship_y} (waypoint at #{new_x},#{new_y})"
    )

    navigate_waypoint(new_ship_x, new_ship_y, new_x, new_y, rest_of_moves)
  end

  @spec step_waypoint(integer(), integer(), integer(), integer(), String.t(), integer()) ::
          {integer(), integer(), integer(), integer()}
  defp step_waypoint(ship_x, ship_y, x, y, "F", amount),
    do: {ship_x + amount * x, ship_y + amount * y, x, y}

  defp step_waypoint(ship_x, ship_y, x, y, "N", amount), do: {ship_x, ship_y, x, y + amount}
  defp step_waypoint(ship_x, ship_y, x, y, "S", amount), do: {ship_x, ship_y, x, y - amount}
  defp step_waypoint(ship_x, ship_y, x, y, "E", amount), do: {ship_x, ship_y, x + amount, y}
  defp step_waypoint(ship_x, ship_y, x, y, "W", amount), do: {ship_x, ship_y, x - amount, y}
  defp step_waypoint(ship_x, ship_y, x, y, "R", 90), do: {ship_x, ship_y, y, -x}
  defp step_waypoint(ship_x, ship_y, x, y, "R", 180), do: {ship_x, ship_y, -x, -y}
  defp step_waypoint(ship_x, ship_y, x, y, "R", 270), do: {ship_x, ship_y, -y, x}

  defp step_waypoint(ship_x, ship_y, x, y, "L", 90),
    do: step_waypoint(ship_x, ship_y, x, y, "R", 270)

  defp step_waypoint(ship_x, ship_y, x, y, "L", 180),
    do: step_waypoint(ship_x, ship_y, x, y, "R", 180)

  defp step_waypoint(ship_x, ship_y, x, y, "L", 270),
    do: step_waypoint(ship_x, ship_y, x, y, "R", 90)
end
