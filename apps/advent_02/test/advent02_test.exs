defmodule Advent02Test do
  use ExUnit.Case
  doctest Advent02

  test "finds_answer_to_first_challenge" do
    passwords =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.split(&1))
      |> Stream.map(&format(&1))
      |> Enum.to_list()

    assert Advent02.valid_passwords(passwords) == 625
  end

  test "finds_answer_to_second_challenge" do
    passwords =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.split(&1))
      |> Stream.map(&format(&1))
      |> Enum.to_list()

    assert Advent02.alternative_valid_passwords(passwords) == 391
  end

  defp format([range, pattern, password]) do
    [min, max] = String.split(range, "-")
    {String.to_integer(min), String.to_integer(max), String.trim_trailing(pattern, ":"), password}
  end
end
