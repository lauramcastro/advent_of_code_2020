defmodule Advent02 do
  @moduledoc """
  Main module of `Advent02`.
  """
  require Logger

  @doc """
  Finding the valid passwords!

  ## Examples

      iex> Advent02.valid_passwords([{1, 3, "a", "abcde"}, {1, 3, "b", "cdefg"}, {2, 9, "c", "ccccccccc"}])
      2

  """
  @spec valid_passwords(list({integer(), integer(), String.t(), String.t()})) :: integer()
  def valid_passwords(list_of_passwords) do
    Task.async_stream(list_of_passwords, fn p -> valid_password(p) end)
    |> Enum.reduce(0, fn
      {:ok, true}, acc -> acc + 1
      {:ok, false}, acc -> acc
    end)
  end

  @doc """
  Finding the valid passwords in a different way!

  ## Examples

      iex> Advent02.alternative_valid_passwords([{1, 3, "a", "abcde"}, {1, 3, "b", "cdefg"}, {2, 9, "c", "ccccccccc"}])
      1

  """
  @spec alternative_valid_passwords(list({integer(), integer(), String.t(), String.t()})) ::
          integer()
  def alternative_valid_passwords(list_of_passwords) do
    Task.async_stream(list_of_passwords, fn p -> also_valid_password(p) end)
    |> Enum.reduce(0, fn
      {:ok, true}, acc -> acc + 1
      {:ok, false}, acc -> acc
    end)
  end

  @spec valid_password({integer(), integer(), String.t(), String.t()}) :: boolean()
  defp valid_password({min, max, char, password}) do
    occurrences(String.to_charlist(char), String.to_charlist(password), 0)
    |> is_valid(min, max)
  end

  @spec also_valid_password({integer(), integer(), String.t(), String.t()}) :: boolean()
  defp also_valid_password({min, max, char, password}) do
    xor(
      String.at(password, min - 1) == char,
      String.at(password, max - 1) == char
    )
  end

  @spec occurrences(charlist(), charlist(), integer()) :: integer()
  defp occurrences(_char, [], found), do: found

  defp occurrences([char], [char | rest_of_string], found),
    do: occurrences([char], rest_of_string, found + 1)

  defp occurrences([char], [_different_char | rest_of_string], found),
    do: occurrences([char], rest_of_string, found)

  @spec is_valid(integer(), integer(), integer()) :: boolean()
  defp is_valid(occurrences, min, max), do: occurrences >= min and occurrences <= max

  @spec xor(boolean(), boolean()) :: boolean()
  defp xor(same, same) when is_boolean(same), do: false
  defp xor(same, different) when is_boolean(same) and is_boolean(different), do: true
end
