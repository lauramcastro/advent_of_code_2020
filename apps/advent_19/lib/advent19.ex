defmodule Advent19 do
  @moduledoc """
  Main module of `Advent19`.
  """

  @doc """
  Automatically generating grammars!

  ## Examples

      # iex> Advent19.generate_grammar("priv/example_rules_1.txt", "parser1")
      # :ok

      # iex> Advent19.generate_grammar("priv/example_rules_2.txt", "parser2")
      # :ok

      # iex> Advent19.generate_grammar("priv/input_rules.txt", "parser")
      # :ok

  """
  @spec generate_grammar(String.t(), String.t()) :: :ok
  def generate_grammar(rules_file, grammar_filename) do
    # generate grammar from from rules in a file
    {nonterminals, productions} =
      Path.expand(rules_file)
      |> Path.absname()
      |> File.stream!()
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()
      |> do_generate_grammar({[], []})

    # we produce the yrl file
    :ok =
      generate_yrl_file(
        "src/" <> grammar_filename <> ".yrl",
        nonterminals,
        ["a", "b"],
        "t0",
        productions
      )
  end

  @doc """
  Checking words against grammars!

  ## Examples

      iex> Advent19.check_words("priv/example_words_1.txt", "parser1")
      2

      iex> Advent19.check_words("priv/example_words_2.txt", "parser2")
      2

  """
  @spec check_words(String.t(), String.t()) :: integer()
  def check_words(words_file, parsername) do
    candidate_words =
      Path.expand(words_file)
      |> Path.absname()
      |> File.stream!()
      |> Stream.map(&String.trim(&1))

    parser = String.to_atom(parsername)

    Task.async_stream(
      candidate_words,
      fn w ->
        {:ok, tokens, _} = :lexer.string(String.to_charlist(w))

        case parser.parse(tokens) do
          {:ok, _ast} -> true
          _ -> false
        end
      end
    )
    |> Enum.reduce(0, fn
      {:ok, true}, acc -> acc + 1
      {:ok, false}, acc -> acc
    end)
  end

  @spec do_generate_grammar(list(String.t()), {list(String.t()), list(String.t())}) ::
          {list(String.t()), list(String.t())}
  defp do_generate_grammar([], grammar_rules), do: grammar_rules

  defp do_generate_grammar([rule | more_rules], {nonterminals, productions}) do
    [nonterminal, production_body] = String.split(rule, ": ")

    new_productions =
      String.split(production_body, " | ")
      |> Enum.map(fn n -> String.split(n) end)
      |> Enum.map(fn
        [e] when e == "a" or e == "b" ->
          {"t" <> nonterminal, "'" <> e <> "' : $1"}

        [p] ->
          {"t" <> nonterminal, "t" <> p <> " : $1"}

        [p1, p2] ->
          {"t" <> nonterminal, "t" <> p1 <> " t" <> p2 <> " : {concat, $1, $2}"}

        # this is here only for example 2!
        [p1, p2, p3] ->
          {"t" <> nonterminal, "t" <> p1 <> " t" <> p2 <> " t" <> p3 <> " : {concat, $1, $2, $3}"}
      end)

    do_generate_grammar(
      more_rules,
      {[nonterminal | nonterminals], productions ++ new_productions}
    )
  end

  @spec generate_yrl_file(
          String.t(),
          list(String.t()),
          list(String.t()),
          String.t(),
          list({String.t(), String.t()})
        ) :: :ok | {:error, atom()}
  defp generate_yrl_file(filename, nonterminals, terminals, root, productions) do
    filecontents =
      "Nonterminals" <>
        Enum.join(for n <- nonterminals, do: " t" <> n) <>
        ".\n" <>
        "Terminals" <>
        Enum.join(for t <- terminals, do: " " <> t) <>
        ".\n" <>
        "Rootsymbol " <>
        root <>
        ".\n" <>
        Enum.join(for {pr, pb} <- productions, do: pr <> " -> " <> pb <> ".\n") <>
        "Erlang code."

    File.write(filename, filecontents)
  end
end
