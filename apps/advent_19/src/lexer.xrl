Definitions.
Rules.
%% letters a and b
a : {token, {'a', TokenLine}}.
b : {token, {'b', TokenLine}}.
%% white space
[\s\n\r\t]+ : skip_token.
%%
Erlang code.