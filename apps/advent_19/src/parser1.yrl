Nonterminals t3 t2 t1 t0.
Terminals a b.
Rootsymbol t0.
t0 -> t1 t2 : {concat, $1, $2}.
t1 -> 'a' : $1.
t2 -> t1 t3 : {concat, $1, $2}.
t2 -> t3 t1 : {concat, $1, $2}.
t3 -> 'b' : $1.
Erlang code.