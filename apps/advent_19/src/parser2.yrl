Nonterminals t5 t4 t3 t2 t1 t0.
Terminals a b.
Rootsymbol t0.
t0 -> t4 t1 t5 : {concat, $1, $2, $3}.
t1 -> t2 t3 : {concat, $1, $2}.
t1 -> t3 t2 : {concat, $1, $2}.
t2 -> t4 t4 : {concat, $1, $2}.
t2 -> t5 t5 : {concat, $1, $2}.
t3 -> t4 t5 : {concat, $1, $2}.
t3 -> t5 t4 : {concat, $1, $2}.
t4 -> 'a' : $1.
t5 -> 'b' : $1.
Erlang code.