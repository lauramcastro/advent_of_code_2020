defmodule Advent14Test do
  use ExUnit.Case
  doctest Advent14

  test "finds_answer_to_first_challenge" do
    [first | rest] =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()

    ["mask", first_mask] = String.split(first, "=") |> Enum.map(&String.trim(&1))

    assert Advent14.process_memory(first_mask, rest, :v1) == 17_028_179_706_934
  end

  test "finds_answer_to_second_challenge" do
    [first | rest] =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()

    ["mask", first_mask] = String.split(first, "=") |> Enum.map(&String.trim(&1))

    assert Advent14.process_memory(first_mask, rest, :v2) == 3_683_236_147_222
  end
end
