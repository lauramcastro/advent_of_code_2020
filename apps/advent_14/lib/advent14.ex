defmodule Advent14 do
  @moduledoc """
  Main module of `Advent14`.
  """

  @doc """
  Updating memory!

  ## Examples

      iex> Advent14.process_memory("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
      ...>                         ["mem[8] = 11",
      ...>                          "mem[7] = 101",
      ...>                          "mem[8] = 0"], :v1)
      165

      iex> Advent14.process_memory("000000000000000000000000000000X1001X",
      ...>                         ["mem[42] = 100",
      ...>                          "mask = 00000000000000000000000000000000X0XX",
      ...>                          "mem[26] = 1"], :v2)
      208

  """
  @spec process_memory(String.t(), list(String.t()), atom()) :: pos_integer()
  def process_memory(initial_mask, instructions, decoder_version),
    do:
      do_process_memory(initial_mask, instructions, Map.new(), decoder_version)
      |> Map.to_list()
      |> Enum.reduce(0, fn {_pos, value}, acc -> value + acc end)

  @spec do_process_memory(String.t(), list(String.t()), map(), atom()) :: map()
  defp do_process_memory(_mask, [], m, _vdec), do: m

  defp do_process_memory(mask, [inst | more_inst], m, vdec) do
    {new_mask, new_m} =
      process_instruction(mask, String.split(inst, "=") |> Enum.map(&String.trim(&1)), m, vdec)

    do_process_memory(new_mask, more_inst, new_m, vdec)
  end

  @spec process_instruction(String.t(), list(String.t()), map(), atom()) :: {String.t(), map()}
  defp process_instruction(_mask, ["mask", new_mask], m, _vdec), do: {new_mask, m}

  defp process_instruction(mask, [mem_pos, value], m, :v1) do
    # decoder version 1: mask applies to value
    ["mem", pos, ""] = String.split(mem_pos, ["[", "]"])
    # apply mask to value
    new_value =
      apply_mask(
        mask,
        String.to_integer(value)
        |> Integer.to_string(2)
        |> String.pad_leading(36, "0")
      )

    # save it into pos in m (if pos exists in m it will be overwritten)
    {mask, Map.put(m, pos, new_value)}
  end

  defp process_instruction(mask, [mem_pos, value], m, :v2) do
    # decoder version 2: mask applies to memory position
    ["mem", pos, ""] = String.split(mem_pos, ["[", "]"])
    # apply mask to pos
    pos_list =
      apply_floating_mask(
        mask,
        String.to_integer(pos)
        |> Integer.to_string(2)
        |> String.pad_leading(36, "0")
      )

    v = String.to_integer(value)
    # save v into each p in pos_list (in m)
    new_m = Enum.reduce(pos_list, m, fn p, m_acc -> Map.put(m_acc, p, v) end)

    {mask, new_m}
  end

  @doc """
  Aplying masks!

  ## Examples

      iex> Advent14.apply_mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
      ...>                     "000000000000000000000000000000001011")
      73

      iex> Advent14.apply_mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
      ...>                     "000000000000000000000000000001100101")
      101

      iex> Advent14.apply_mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
      ...>                     "000000000000000000000000000000000000")
      64

  """
  @spec apply_mask(String.t(), String.t()) :: pos_integer()
  def apply_mask(mask, value),
    do:
      do_apply_mask(
        String.graphemes(mask),
        String.graphemes(value),
        []
      )
      |> Enum.join()
      |> String.to_integer(2)

  @spec do_apply_mask(list(String.t()), list(String.t()), list(String.t())) :: list(String.t())
  defp do_apply_mask([], [], value), do: Enum.reverse(value)

  defp do_apply_mask(["X" | more_mask], [v | more_value], acc),
    do: do_apply_mask(more_mask, more_value, [v | acc])

  defp do_apply_mask(["1" | more_mask], [_ | more_value], acc),
    do: do_apply_mask(more_mask, more_value, ["1" | acc])

  defp do_apply_mask(["0" | more_mask], [_ | more_value], acc),
    do: do_apply_mask(more_mask, more_value, ["0" | acc])

  @doc """
  Aplying floating masks!

  ## Examples

      iex> Advent14.apply_floating_mask("000000000000000000000000000000X1001X",
      ...>                              "000000000000000000000000000000101010")
      [26, 27, 58, 59]

      iex> Advent14.apply_floating_mask("00000000000000000000000000000000X0XX",
      ...>                              "000000000000000000000000000000011010")
      [16, 17, 18, 19, 24, 25, 26, 27]

  """
  @spec apply_floating_mask(String.t(), String.t()) :: list(pos_integer())
  def apply_floating_mask(mask, value),
    do:
      do_apply_floating_mask(
        String.graphemes(mask),
        String.graphemes(value),
        []
      )
      |> Enum.join()
      |> variations()
      |> Enum.sort()

  @spec do_apply_floating_mask(list(String.t()), list(String.t()), list(String.t())) ::
          list(String.t())
  defp do_apply_floating_mask([], [], value), do: Enum.reverse(value)

  defp do_apply_floating_mask(["X" | more_mask], [_ | more_value], acc),
    do: do_apply_floating_mask(more_mask, more_value, ["X" | acc])

  defp do_apply_floating_mask(["1" | more_mask], [_ | more_value], acc),
    do: do_apply_floating_mask(more_mask, more_value, ["1" | acc])

  defp do_apply_floating_mask(["0" | more_mask], [v | more_value], acc),
    do: do_apply_floating_mask(more_mask, more_value, [v | acc])

  @spec variations(String.t()) :: list(pos_integer())
  defp variations(c) do
    do_variations([c], []) |> Enum.map(fn v -> String.to_integer(v, 2) end)
  end

  @spec do_variations(list(String.t()), list(String.t())) :: list(String.t())
  defp do_variations([], variations), do: variations

  defp do_variations([c | more_c], variations) do
    {new_c, new_variations} =
      if String.contains?(c, "X"),
        do:
          {[
             String.replace(c, "X", "0", global: false),
             String.replace(c, "X", "1", global: false) | more_c
           ], variations},
        else: {more_c, [c | variations]}

    do_variations(new_c, new_variations)
  end
end
