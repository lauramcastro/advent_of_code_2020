defmodule Advent15Test do
  use ExUnit.Case
  doctest Advent15

  test "finds_answer_to_first_challenge" do
    assert Advent15.play_elves_game([6, 3, 15, 13, 1, 0], 2020) == 700
  end

  @tag timeout: :infinity
  test "brute_forcing_second_challenge" do
    assert Advent15.play_elves_game([6, 3, 15, 13, 1, 0], 30_000_000) == 51_358
  end
end
