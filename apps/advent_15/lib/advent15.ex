defmodule Advent15 do
  @moduledoc """
  Main module of `Advent15`.
  """

  require Logger

  @doc """
  Playing with the Elves!

  ## Examples

      iex> Advent15.play_elves_game([0, 3, 6], 1)
      0

      iex> Advent15.play_elves_game([0, 3, 6], 2)
      3

      iex> Advent15.play_elves_game([0, 3, 6], 3)
      6

      iex> Advent15.play_elves_game([0, 3, 6], 4)
      0

      iex> Advent15.play_elves_game([0, 3, 6], 5)
      3

      iex> Advent15.play_elves_game([0, 3, 6], 6)
      3

      iex> Advent15.play_elves_game([0, 3, 6], 7)
      1

      iex> Advent15.play_elves_game([0, 3, 6], 8)
      0

      iex> Advent15.play_elves_game([0, 3, 6], 9)
      4

      iex> Advent15.play_elves_game([0, 3, 6], 10)
      0

      iex> Advent15.play_elves_game([0, 3, 6], 2020)
      436
      
      iex> Advent15.play_elves_game([1, 3, 2], 2020)
      1

      iex> Advent15.play_elves_game([2, 1, 3], 2020)
      10

      iex> Advent15.play_elves_game([1, 2, 3], 2020)
      27

      iex> Advent15.play_elves_game([2, 3, 1], 2020)
      78

      iex> Advent15.play_elves_game([3, 2, 1], 2020)
      438

      iex> Advent15.play_elves_game([3, 1, 2], 2020)
      1836

  """
  @spec play_elves_game(list(integer()), integer()) :: integer()
  def play_elves_game(starting_numbers, turns),
    do: do_play_elves_game(0, turns, starting_numbers, hd(starting_numbers), Map.new())

  @spec do_play_elves_game(
          integer(),
          integer(),
          nonempty_list(integer()),
          integer(),
          map()
        ) ::
          integer()
  defp do_play_elves_game(turns, turns, _starting_numbers, last_spoken, _numbers), do: last_spoken

  defp do_play_elves_game(turn, turns, starting_numbers, last_spoken, numbers)
       when turn < length(starting_numbers) do
    spoken_number = Enum.at(starting_numbers, turn)
    Logger.debug("Turn #{turn + 1}, spoken number #{spoken_number}")

    do_play_elves_game(
      turn + 1,
      turns,
      starting_numbers,
      spoken_number,
      Map.put(numbers, last_spoken, turn)
    )
  end

  defp do_play_elves_game(turn, turns, starting_numbers, last_spoken, numbers) do
    if Map.has_key?(numbers, last_spoken) do
      # last number spoken had been spoken before, return turns difference
      previous_turn = Map.get(numbers, last_spoken)
      Logger.debug("Turn #{turn + 1}, spoken number #{turn - previous_turn}")

      do_play_elves_game(
        turn + 1,
        turns,
        starting_numbers,
        turn - previous_turn,
        Map.put(numbers, last_spoken, turn)
      )
    else
      # last number spoken was a first timer
      Logger.debug("Turn #{turn + 1}, spoken number 0")

      do_play_elves_game(
        turn + 1,
        turns,
        starting_numbers,
        0,
        Map.put(numbers, last_spoken, turn)
      )
    end
  end
end
