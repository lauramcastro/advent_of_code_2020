defmodule Advent11 do
  @moduledoc """
  Main module of `Advent11`.
  """

  require Logger

  @doc """
  Counting seats!

  ## Examples

      iex> Advent11.occupied(["L.LL.LL.LL",
      ...>                    "LLLLLLL.LL",
      ...>                    "L.L.L..L..",
      ...>                    "LLLL.LL.LL",
      ...>                    "L.LL.LL.LL",
      ...>                    "L.LLLLL.LL",
      ...>                    "..L.L.....",
      ...>                    "LLLLLLLLLL",
      ...>                    "L.LLLLLL.L",
      ...>                    "L.LLLLL.LL"])
      0

  """
  @spec occupied(list(String.t())) :: pos_integer()
  def occupied(lounge) do
    process(lounge) |> Matrex.new() |> Matrex.to_list() |> Enum.count(fn x -> x == 1 end)
  end

  @doc """
  Counting seats after some movement!

  ## Examples

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 1, :adjacent)
      71

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 2, :adjacent)
      20

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 3, :adjacent)
      51

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 4, :adjacent)
      30

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 5, :adjacent)
      37

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 6, :adjacent)
      37

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 1, :visible)
      71

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 2, :visible)
      7
      
      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 3, :visible)
      53

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 4, :visible)
      18

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 5, :visible)
      31

      iex> Advent11.occupied_after(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], 6, :visible)
      26

  """
  @spec occupied_after(list(String.t()), pos_integer(), atom()) :: pos_integer()
  def occupied_after(lounge, rounds, neighbours) do
    process(lounge)
    |> Matrex.new()
    |> custom_conway(rounds, neighbours)
    |> Matrex.to_list()
    |> Enum.count(fn x -> x == 1 end)
  end

  @doc """
  Counting seats after movement stabilizes!

  ## Examples

      iex> Advent11.occupied_final(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], :adjacent)
      37

      iex> Advent11.occupied_final(["L.LL.LL.LL",
      ...>                          "LLLLLLL.LL",
      ...>                          "L.L.L..L..",
      ...>                          "LLLL.LL.LL",
      ...>                          "L.LL.LL.LL",
      ...>                          "L.LLLLL.LL",
      ...>                          "..L.L.....",
      ...>                          "LLLLLLLLLL",
      ...>                          "L.LLLLLL.L",
      ...>                          "L.LLLLL.LL"], :visible)
      26

  """
  @spec occupied_final(list(String.t()), atom()) :: pos_integer()
  def occupied_final(lounge, neighbours) do
    process(lounge)
    |> Matrex.new()
    |> custom_conway(false, neighbours)
    |> Matrex.to_list()
    |> Enum.count(fn x -> x == 1 end)
  end

  @spec process(list(String.t())) :: list(list(integer()))
  defp process(lounge) do
    lounge |> Enum.map(fn row -> String.graphemes(row) |> Enum.map(fn e -> encode(e) end) end)
  end

  @spec encode(String.t()) :: integer()
  defp encode("."), do: -1
  defp encode("L"), do: 0
  defp encode("#"), do: 1

  @spec custom_conway(Matrex.t(), pos_integer() | boolean, atom()) :: Matrex.t()
  defp custom_conway(m, true, _neighbours) do
    # Matrex.print(m)
    m
  end

  defp custom_conway(m, false, neighbours) do
    # Matrex.print(m)

    new_m =
      do_custom_conway(m, 1, elem(Matrex.size(m), 0), 1, elem(Matrex.size(m), 1), m, neighbours)

    custom_conway(new_m, m == new_m, neighbours)
  end

  defp custom_conway(m, 0, _neighbours) do
    # Matrex.print(m)
    m
  end

  defp custom_conway(m, n, neighbours) do
    # Matrex.print(m)

    custom_conway(
      do_custom_conway(
        m,
        1,
        elem(Matrex.size(m), 0),
        1,
        elem(Matrex.size(m), 1),
        m,
        neighbours
      ),
      n - 1,
      neighbours
    )
  end

  @spec do_custom_conway(
          Matrex.t(),
          pos_integer(),
          pos_integer(),
          pos_integer(),
          pos_integer(),
          Matrex.t(),
          atom()
        ) :: Matrex.t()
  defp do_custom_conway(current, r_max, r_max, c_max, c_max, next, neighbours),
    do:
      Matrex.set(
        next,
        r_max,
        c_max,
        next_conway_value(
          Matrex.at(current, r_max, c_max),
          r_max,
          c_max,
          current,
          neighbours
        )
      )

  defp do_custom_conway(current, r, r_max, c_max, c_max, next, neighbours),
    do:
      do_custom_conway(
        current,
        r + 1,
        r_max,
        1,
        c_max,
        Matrex.set(
          next,
          r,
          c_max,
          next_conway_value(
            Matrex.at(current, r, c_max),
            r,
            c_max,
            current,
            neighbours
          )
        ),
        neighbours
      )

  defp do_custom_conway(current, r, r_max, c, c_max, next, neighbours),
    do:
      do_custom_conway(
        current,
        r,
        r_max,
        c + 1,
        c_max,
        Matrex.set(
          next,
          r,
          c,
          next_conway_value(Matrex.at(current, r, c), r, c, current, neighbours)
        ),
        neighbours
      )

  @spec next_conway_value(
          float(),
          pos_integer(),
          pos_integer(),
          Matrex.t(),
          atom()
        ) :: float()
  defp next_conway_value(-1.0, r, c, _m, _neighbours) do
    Logger.debug("Floor at #{r}, #{c} unchanged")
    -1
  end

  defp next_conway_value(0.0, r, c, m, neighbours) do
    new_value =
      take_seat(
        0.0,
        apply(__MODULE__, neighbours, [m, r, c])
        |> Enum.filter(fn x -> x != -1.0 and x != nil end)
        |> Enum.all?(fn x -> x == 0.0 end)
      )

    Logger.debug("Free at #{r},#{c} is now #{new_value}")
    new_value
  end

  defp next_conway_value(1.0, r, c, m, neighbours) do
    new_value =
      take_seat(
        1.0,
        apply(__MODULE__, neighbours, [m, r, c])
        |> Enum.filter(fn x -> x != -1.0 and x != nil end)
        |> Enum.count(fn x -> x == 1.0 end) < tolerance(neighbours)
      )

    Logger.debug("Taken seat at #{r},#{c} is now #{new_value}")
    new_value
  end

  @spec adjacent(Matrex.t(), pos_integer(), pos_integer()) ::
          list(float())
  def adjacent(m, r, c),
    do: [
      safe_access(m, r - 1, c - 1),
      safe_access(m, r - 1, c),
      safe_access(m, r - 1, c + 1),
      safe_access(m, r, c - 1),
      safe_access(m, r, c + 1),
      safe_access(m, r + 1, c - 1),
      safe_access(m, r + 1, c),
      safe_access(m, r + 1, c + 1)
    ]

  @spec visible(Matrex.t(), pos_integer(), pos_integer()) :: list(float())
  def visible(m, r, c) do
    size = Matrex.size(m)

    same_row_right(m, r, c, size) ++
      same_row_left(m, r, c, size) ++
      same_column_up(m, r, c, size) ++
      same_column_down(m, r, c, size) ++
      same_diagonal_right_up(m, r, c, size) ++
      same_diagonal_right_down(m, r, c, size) ++
      same_diagonal_left_up(m, r, c, size) ++
      same_diagonal_left_down(m, r, c, size)
  end

  @spec same_row_right(Matrex.t(), pos_integer(), pos_integer(), {integer(), integer()}) ::
          list(float())
  def same_row_right(m, r, c, {_, c_max}) do
    Enum.reduce_while(
      (c + 1)..c_max,
      [],
      fn x, acc ->
        value = safe_access(m, r, x)
        Logger.debug("---> RR: Value at #{r},#{x} is #{value}")

        if value != nil and value == -1.0,
          do: {:cont, [value | acc]},
          else: {:halt, [value | acc]}
      end
    )
  end

  @spec same_row_left(Matrex.t(), pos_integer(), pos_integer(), {integer(), integer()}) ::
          list(float())
  def same_row_left(m, r, c, _),
    do:
      Enum.reduce_while(
        (c - 1)..1,
        [],
        fn x, acc ->
          value = safe_access(m, r, x)
          Logger.debug("---> RL: Value at #{r},#{x} is #{value}")

          if value != nil and value == -1.0,
            do: {:cont, [value | acc]},
            else: {:halt, [value | acc]}
        end
      )

  @spec same_column_up(Matrex.t(), pos_integer(), pos_integer(), {integer(), integer()}) ::
          list(float())
  def same_column_up(m, r, c, _),
    do:
      Enum.reduce_while(
        (r - 1)..1,
        [],
        fn x, acc ->
          value = safe_access(m, x, c)
          Logger.debug("---> CU: Value at #{x},#{c} is #{value}")

          if value != nil and value == -1.0,
            do: {:cont, [value | acc]},
            else: {:halt, [value | acc]}
        end
      )

  @spec same_column_down(Matrex.t(), pos_integer(), pos_integer(), {integer(), integer()}) ::
          list(float())
  def same_column_down(m, r, c, {r_max, _}),
    do:
      Enum.reduce_while(
        (r + 1)..r_max,
        [],
        fn x, acc ->
          value = safe_access(m, x, c)
          Logger.debug("---> CD: Value at #{x},#{c} is #{value}")

          if value != nil and value == -1.0,
            do: {:cont, [value | acc]},
            else: {:halt, [value | acc]}
        end
      )

  @spec same_diagonal_right_up(Matrex.t(), pos_integer(), pos_integer(), {integer(), integer()}) ::
          list(float())
  def same_diagonal_right_up(m, r, c, {_, c_max}),
    do:
      Enum.reduce_while(
        List.zip([Enum.to_list((r - 1)..1), Enum.to_list((c + 1)..c_max)]),
        [],
        fn {x, y}, acc ->
          value = safe_access(m, x, y)
          Logger.debug("---> DRU: Value at #{x},#{y} is #{value}")

          if value != nil and value == -1.0,
            do: {:cont, [value | acc]},
            else: {:halt, [value | acc]}
        end
      )

  @spec same_diagonal_right_down(Matrex.t(), pos_integer(), pos_integer(), {integer(), integer()}) ::
          list(float())
  def same_diagonal_right_down(m, r, c, {r_max, c_max}),
    do:
      Enum.reduce_while(
        List.zip([Enum.to_list((r + 1)..r_max), Enum.to_list((c + 1)..c_max)]),
        [],
        fn {x, y}, acc ->
          value = safe_access(m, x, y)
          Logger.debug("---> DRD: Value at #{x},#{y} is #{value}")

          if value != nil and value == -1.0,
            do: {:cont, [value | acc]},
            else: {:halt, [value | acc]}
        end
      )

  @spec same_diagonal_left_up(Matrex.t(), pos_integer(), pos_integer(), {integer(), integer()}) ::
          list(float())
  def same_diagonal_left_up(m, r, c, _),
    do:
      Enum.reduce_while(
        List.zip([Enum.to_list((r - 1)..1), Enum.to_list((c - 1)..1)]),
        [],
        fn {x, y}, acc ->
          value = safe_access(m, x, y)
          Logger.debug("---> DLU: Value at #{x},#{y} is #{value}")

          if value != nil and value == -1.0,
            do: {:cont, [value | acc]},
            else: {:halt, [value | acc]}
        end
      )

  @spec same_diagonal_left_down(Matrex.t(), pos_integer(), pos_integer(), {integer(), integer()}) ::
          list(float())
  def same_diagonal_left_down(m, r, c, {r_max, _}),
    do:
      Enum.reduce_while(
        List.zip([Enum.to_list((r + 1)..r_max), Enum.to_list((c - 1)..1)]),
        [],
        fn {x, y}, acc ->
          value = safe_access(m, x, y)
          Logger.debug("---> DLD: Value at #{x},#{y} is #{value}")

          if value != nil and value == -1.0,
            do: {:cont, [value | acc]},
            else: {:halt, [value | acc]}
        end
      )

  @spec safe_access(Matrex.t(), pos_integer(), pos_integer()) :: float() | nil
  defp safe_access(m, r, c) do
    try do
      Matrex.at(m, r, c)
    rescue
      _ in ArgumentError -> nil
    end
  end

  @spec take_seat(float(), boolean()) :: float()
  defp take_seat(0.0, true), do: 1.0
  defp take_seat(0.0, false), do: 0.0
  defp take_seat(1.0, true), do: 1.0
  defp take_seat(1.0, false), do: 0.0

  @spec tolerance(atom()) :: pos_integer()
  defp tolerance(:adjacent), do: 4
  defp tolerance(:visible), do: 5
end
