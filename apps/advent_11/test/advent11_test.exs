defmodule Advent11Test do
  use ExUnit.Case
  doctest Advent11

  @tag timeout: :infinity
  test "finds_answer_to_first_challenge" do
    lounge =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))

    assert Advent11.occupied_final(lounge, :adjacent) == 2361
  end

  @tag timeout: :infinity
  test "finds_answer_to_second_challenge" do
    lounge =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))

    assert Advent11.occupied_final(lounge, :visible) == 2119
  end
end
