defmodule Advent07 do
  @moduledoc """
  Main module of `Advent07`.
  """

  @doc """
  Find holder bags!

  # Examples

      iex> Advent07.can_hold(["light red bags contain 1 bright white bag, 2 muted yellow bags.",
      ...>                    "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
      ...>                    "bright white bags contain 1 shiny gold bag.",
      ...>                    "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
      ...>                    "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
      ...>                    "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
      ...>                    "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
      ...>                    "faded blue bags contain no other bags.",
      ...>                    "dotted black bags contain no other bags."
      ...>                   ], "shiny gold bag")
      4

  """
  @spec can_hold(list(String.t()), String.t()) :: integer()
  def can_hold(list_of_bags, bag) do
    can_hold(list_of_bags, [bag], MapSet.new())
  end

  @spec can_hold(list(String.t()), list(String.t()), MapSet.t()) :: integer()
  defp can_hold(_list_of_bags, [], known_bags), do: MapSet.size(known_bags) - 1

  defp can_hold(list_of_bags, [bag | rest_of_bags], known_bags) do
    potential_holders =
      Enum.filter(list_of_bags, fn s ->
        String.contains?(s, bag) and not String.starts_with?(s, bag)
      end)
      |> Enum.map(fn s -> hd(String.split(s, "s contain ")) end)

    # do not check bags that have already been or will be checked
    holders =
      MapSet.difference(
        MapSet.new(potential_holders),
        MapSet.union(MapSet.new(rest_of_bags), known_bags)
      )
      |> MapSet.to_list()

    # recursively call can_hold on new holders plus pending
    can_hold(list_of_bags, rest_of_bags ++ holders, MapSet.put(known_bags, bag))
  end

  @doc """
  Find held bags!

  # Examples

      # iex> Advent07.can_be_held(["light red bags contain 1 bright white bag, 2 muted yellow bags.",
      # ...>                       "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
      # ...>                       "bright white bags contain 1 shiny gold bag.",
      # ...>                       "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
      # ...>                       "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
      # ...>                       "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
      # ...>                       "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
      # ...>                       "faded blue bags contain no other bags.",
      # ...>                       "dotted black bags contain no other bags."
      # ...>                      ], "shiny gold bag")
      # 33

      # iex> Advent07.can_be_held(["shiny gold bags contain 2 dark red bags.",
      # ...>                       "dark red bags contain 2 dark orange bags.",
      # ...>                       "dark orange bags contain 2 dark yellow bags.",
      # ...>                       "dark yellow bags contain 2 dark green bags.",
      # ...>                       "dark green bags contain 2 dark blue bags.",
      # ...>                       "dark blue bags contain 2 dark violet bags.",
      # ...>                       "dark violet bags contain no other bags."
      # ...>                      ], "shiny gold bag")
      # 127

  """
  @spec can_be_held(list(String.t()), String.t()) :: integer()
  def can_be_held(list_of_bags, bag) do
    contents =
      Enum.filter(list_of_bags, fn s ->
        String.contains?(s, bag <> "s contain ") and not String.contains?(s, "no other bags.")
      end)
      |> Enum.flat_map(fn s ->
        String.replace_prefix(s, bag <> "s contain ", "")
        |> String.split(",")
        |> Enum.map(fn b ->
          {ns, bs} = String.trim(b) |> String.next_grapheme()

          {String.to_integer(ns),
           String.trim(bs) |> String.replace_suffix(".", "") |> String.replace_suffix("s", "")}
        end)
      end)

    # recursively call can_be_held on contents (no tail-recursive version this time...)
    Task.async_stream(contents, fn {nc, c} -> {nc, can_be_held(list_of_bags, c)} end)
    |> Enum.reduce(1, fn {:ok, {x, y}}, acc -> acc + x * y end)
  end
end
