defmodule Advent07Test do
  use ExUnit.Case
  doctest Advent07

  test "finds_answer_to_first_challenge" do
    list_of_bags =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Enum.to_list()

    assert Advent07.can_hold(list_of_bags, "shiny gold bag") == 192
  end

  test "finds_answer_to_second_challenge" do
    list_of_bags =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Enum.to_list()

    assert Advent07.can_be_held(list_of_bags, "shiny gold bag") == 12_129
  end
end
