defmodule Advent18Test do
  use ExUnit.Case
  doctest Advent18

  test "finds_answer_to_first_challenge" do
    expressions =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))

    assert Advent18.evaluate_all(expressions, :sequential) == 21_347_713_555_555
  end

  test "finds_answer_to_second_challenge" do
    expressions =
      Path.expand("priv/input.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))

    assert Advent18.evaluate_all(expressions, :precedence) == 275_011_754_427_339
  end
end
