defmodule Advent18 do
  @moduledoc """
  Main module of `Advent18`.
  """

  require Logger

  @doc """
  Evaluating math expressions!

  ## Examples

      iex> Advent18.evaluate_all(["1 + 2 * 3 + 4 * 5 + 6",
      ...>                        "1 + (2  * 3) + (4 * (5 + 6))",
      ...>                        "2 * 3 + (4 * 5)",
      ...>                        "5 + (8 * 3 + 9 + 3 * 4 * 3)",
      ...>                        "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))",
      ...>                        "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"], :sequential)
      26457

  """
  @spec evaluate_all(list(String.t()), atom()) :: integer()
  def evaluate_all(expressions, precedence) do
    Task.async_stream(expressions, fn e -> evaluate(e, precedence) end)
    |> Enum.reduce(0, fn {:ok, sum}, acc -> sum + acc end)
  end

  @doc """
  Evaluating math expressions!

  ## Examples

      iex> Advent18.evaluate("1 + 2 * 3 + 4 * 5 + 6", :sequential)
      71

      iex> Advent18.evaluate("1 + (2 * 3) + (4 * (5 + 6))", :sequential)
      51

      iex> Advent18.evaluate("2 * 3 + (4 * 5)", :sequential)
      26

      iex> Advent18.evaluate("5 + (8 * 3 + 9 + 3 * 4 * 3)", :sequential)
      437

      iex> Advent18.evaluate("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", :sequential)
      12240

      iex> Advent18.evaluate("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", :sequential)
      13632

      iex> Advent18.evaluate("1 + 2 * 3 + 4 * 5 + 6", :precedence)
      231

      # iex> Advent18.evaluate("1 + (2 * 3) + (4 * (5 + 6))", :precedence)
      # 51

      iex> Advent18.evaluate("2 * 3 + (4 * 5)", :precedence)
      46

      iex> Advent18.evaluate("5 + (8 * 3 + 9 + 3 * 4 * 3)", :precedence)
      1445

      iex> Advent18.evaluate("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", :precedence)
      669060

      iex> Advent18.evaluate("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", :precedence)
      23340

  """
  @spec evaluate(String.t(), atom()) :: integer()
  def evaluate(expression, :sequential),
    do:
      String.graphemes(expression)
      |> Enum.filter(fn e -> e != " " end)
      |> Enum.map(fn x -> to_integer(x) end)
      |> do_evaluate(nil)
      |> elem(0)

  def evaluate(expression, :precedence) do
    # finally use leex and yecc as one should have from the start
    {:ok, tokens, _} = :lexer.string(String.to_charlist(expression))
    {:ok, ast} = :parser.parse(tokens)
    do_evaluate_ast(ast, 0)
  end

  @spec to_integer(String.t()) :: String.t() | integer()
  defp to_integer(e) when e == "+" or e == "*" or e == "(" or e == ")", do: e
  defp to_integer(e), do: String.to_integer(e)

  defp do_evaluate([], acc), do: {acc, []}

  defp do_evaluate([")"], acc), do: {acc, []}

  defp do_evaluate([")" | rest_of_elements], acc), do: {acc, rest_of_elements}

  defp do_evaluate(["(" | rest_of_elements], acc) do
    Logger.debug("Starting sub-expression #{inspect(rest_of_elements)}")
    {value, more_elements} = do_evaluate(rest_of_elements, nil)
    do_evaluate([value | more_elements], acc)
  end

  defp do_evaluate(["+", f | rest_of_elements], acc) when is_integer(f) do
    Logger.debug("Adding #{f}")
    do_evaluate(rest_of_elements, acc + f)
  end

  defp do_evaluate(["*", f | rest_of_elements], acc) when is_integer(f) do
    Logger.debug("Multiplying #{f}")
    do_evaluate(rest_of_elements, acc * f)
  end

  defp do_evaluate([f | rest_of_elements], nil) when is_integer(f) do
    Logger.debug("Accumulating #{f}")
    do_evaluate(rest_of_elements, f)
  end

  defp do_evaluate([op, "(" | rest_of_elements], acc) do
    Logger.debug("Starting sub-expression #{inspect(rest_of_elements)}")
    {value, more_elements} = do_evaluate(rest_of_elements, nil)
    do_evaluate([op, value | more_elements], acc)
  end

  defp do_evaluate_ast([], acc), do: acc
  defp do_evaluate_ast({:number, _, n}, _acc), do: n

  defp do_evaluate_ast({:plus, lhs, rhs}, acc),
    do: do_evaluate_ast(lhs, acc) + do_evaluate_ast(rhs, acc)

  defp do_evaluate_ast({:mult, lhs, rhs}, acc),
    do: do_evaluate_ast(lhs, acc) * do_evaluate_ast(rhs, acc)
end
