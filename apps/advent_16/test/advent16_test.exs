defmodule Advent16Test do
  use ExUnit.Case
  doctest Advent16

  test "finds_answer_to_first_challenge" do
    rules =
      Path.expand("priv/input_rules.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Enum.to_list()

    tickets =
      Path.expand("priv/input_tickets.txt")
      |> Path.absname()
      |> File.stream!([:read])
      |> Stream.map(&String.trim(&1))
      |> Stream.map(&String.split(&1, ","))
      |> Stream.map(&to_integer(&1))
      |> Enum.to_list()

    assert Advent16.error_rate(rules, tickets) == 24_021
  end

  # test "finds_answer_to_second_challenge" do
  #   rules =
  #     Path.expand("priv/input_rules.txt")
  #     |> Path.absname()
  #     |> File.stream!([:read])
  #     |> Stream.map(&String.trim(&1))
  #     |> Enum.to_list()

  #   tickets =
  #     Path.expand("priv/input_tickets.txt")
  #     |> Path.absname()
  #     |> File.stream!([:read])
  #     |> Stream.map(&String.trim(&1))
  #     |> Stream.map(&String.split(&1, ","))
  #     |> Stream.map(&to_integer(&1))
  #     |> Enum.to_list()

  #   assert Advent16.sort_rules(rules, tickets) == :unknown
  # end

  defp to_integer(list), do: Stream.map(list, &String.to_integer(&1)) |> Enum.to_list()
end
