defmodule Advent16 do
  @moduledoc """
  Main module of `Advent16`.
  """

  require Logger

  @doc """
  Validating tickets!

  ## Examples

      iex> Advent16.error_rate(["class: 1-3 or 5-7",
      ...>                      "row: 6-11 or 33-44",
      ...>                      "seat: 13-40 or 45-50"],
      ...>                      [[7,3,47],
      ...>                       [40,4,50],
      ...>                       [55,2,20],
      ...>                       [38,6,12]])
      71

  """
  @spec error_rate(list(String.t()), list(list(integer()))) :: integer()
  def error_rate(rules, tickets) do
    ranges = decode(rules, [])

    Enum.map(tickets, fn t -> fulfills_rules(t, ranges) end)
    |> Enum.reduce(0, fn
      true, acc -> acc
      {false, offending_field}, acc -> offending_field + acc
    end)
  end

  @doc """
  Sorting out ticket fields!

  ## Examples

      iex> Advent16.sort_rules(["class: 0-1 or 4-19",
      ...>                      "row: 0-5 or 8-19",
      ...>                      "seat: 0-13 or 16-19"],
      ...>                      [[3,9,18],
      ...>                       [15,1,5],
      ...>                       [5,14,9]])
      ["row", "class", "seat"]

      # iex> Advent16.sort_rules(["class: 1-3 or 5-7",
      # ...>                      "row: 6-11 or 33-44",
      # ...>                      "seat: 13-40 or 45-50"],
      # ...>                     [[7,3,47],
      # ...>                      [40,4,50],
      # ...>                      [55,2,20],
      # ...>                      [38,6,12]])
      # ["row", "class", "seat"]

  """
  @spec sort_rules(list(String.t()), list(list(integer()))) :: list(String.t())
  def sort_rules(rules, tickets) do
    ranges = decode(rules, [])

    valid_tickets =
      Enum.filter(tickets, fn t ->
        case fulfills_rules(t, ranges) do
          true -> true
          {false, _offending_field} -> false
        end
      end)

    do_sort_rules(valid_tickets, [], ranges, [], [])
    |> Enum.map(fn {field, _low_range, _high_range} -> field end)
  end

  @spec decode(list(String.t()), list({String.t(), Range.t(), Range.t()})) ::
          list({String.t(), Range.t(), Range.t()})
  defp decode([], ranges), do: Enum.reverse(ranges)

  defp decode([rule | rest_of_rules], ranges) do
    [field, rule_ranges] = String.split(rule, ": ")
    [low_range, high_range] = String.split(rule_ranges, " or ")
    [min_low, max_low] = String.split(low_range, "-")
    [min_high, max_high] = String.split(high_range, "-")

    decoded_rule =
      {field, String.to_integer(min_low)..String.to_integer(max_low),
       String.to_integer(min_high)..String.to_integer(max_high)}

    decode(rest_of_rules, [decoded_rule | ranges])
  end

  @spec fulfills_rules(list(integer()), list({String.t(), Range.t(), Range.t()})) ::
          true | {false, integer()}
  defp fulfills_rules([], _rules), do: true

  defp fulfills_rules([field | rest_of_fields], rules) do
    case(fulfills_ranges(field, rules)) do
      true -> fulfills_rules(rest_of_fields, rules)
      {false, offending_value} -> {false, offending_value}
    end
  end

  @spec fulfills_ranges(integer(), list({String.t(), Range.t(), Range.t()})) ::
          true | {false, integer()}
  defp fulfills_ranges(field, []), do: {false, field}

  defp fulfills_ranges(field, [{_, min_low..max_low, min_high..max_high} | _rest_of_ranges])
       when (field >= min_low and field <= max_low) or (field >= min_high and field <= max_high),
       do: true

  defp fulfills_ranges(field, [_range | rest_of_ranges]),
    do: fulfills_ranges(field, rest_of_ranges)

  @spec do_sort_rules(
          list(list(integer())),
          list(list(integer())),
          list({String.t(), Range.t(), Range.t()}),
          list({String.t(), Range.t(), Range.t()}),
          list(String.t())
        ) ::
          list(String.t())
  defp do_sort_rules(_, _, [], [], sorted_fields) do
    Logger.debug("No more rules nor pending rules, we are done")
    Enum.reverse(sorted_fields)
  end

  defp do_sort_rules([[] | _], [last_ticket | scanned_tickets], [], pending_rules, [
         last_sorted | sorted_rules
       ]) do
    Logger.debug("No more tickets but pending rules, we have to backtrack")

    do_sort_rules(
      [last_ticket],
      scanned_tickets,
      pending_rules ++ [last_sorted],
      [],
      sorted_rules
    )
  end

  defp do_sort_rules(tickets, scanned_tickets, [], pending_rules, sorted_rules) do
    Logger.debug(
      "No more rules but pending rules and pending tickets, we give the pending another round"
    )

    do_sort_rules(
      tickets,
      scanned_tickets,
      Enum.reverse(pending_rules),
      [],
      sorted_rules
    )
  end

  defp do_sort_rules(
         tickets,
         reviewed_tickets,
         [rule | rest_of_rules],
         pending_rules,
         sorted_rules
       ) do
    {fields, more_fields} =
      Enum.map_reduce(tickets, [], fn
        [], acc -> {nil, acc}
        [h | t], acc -> {h, [t | acc]}
      end)

    if Enum.all?(fields, fn f ->
         case fulfills_ranges(f, [rule]) do
           true -> true
           {false, _offending_field} -> false
         end
       end) do
      Logger.debug(
        "Rule #{inspect(rule)} is fulfilled by all #{inspect(fields)}, it is added to sorted_rules"
      )

      do_sort_rules(more_fields, [fields | reviewed_tickets], rest_of_rules, pending_rules, [
        rule | sorted_rules
      ])
    else
      Logger.debug(
        "Rule #{inspect(rule)} is not fulfilled by all #{inspect(fields)} we try the next and add this one to pending"
      )

      do_sort_rules(
        tickets,
        reviewed_tickets,
        rest_of_rules,
        [rule | pending_rules],
        sorted_rules
      )
    end
  end
end
